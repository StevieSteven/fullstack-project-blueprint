import java.nio.charset.StandardCharsets

val buildVersion = File(project.projectDir, "version").readText(StandardCharsets.UTF_8).trim()
println("building version: $buildVersion")

group = "de.stephanstrehler"
version = buildVersion

plugins {
	kotlin("jvm") version "1.9.10"
}

repositories {
	mavenCentral()
}

java.sourceCompatibility = JavaVersion.VERSION_17

allprojects {
	group = "de.stephanstrehler"
	version = buildVersion
}
