# fullstack-project-blueprint

This repository is an example project for starting your fullstack project. 

In the following you can find an overview about the technologies. For more information visit [the documentation folder of this project](documentation/index.md).

## Technology summary

The following technologies are used in this project.

### backend

* [Gradle](https://gradle.org/) 
* [Kotlin](https://kotlinlang.org/)
* [SpringBoot](https://spring.io/projects/spring-boot)
* [Liquibase](https://www.liquibase.org/)

### frontend

* [TypeScript](https://www.typescriptlang.org/)
* [React](https://reactjs.org/)
* [tanstack-query](https://tanstack.com/query/latest)
* [Mui](https://mui.com/)
* [i18next](https://www.i18next.com/)

### infrastructure

* [Gitlab CI](https://docs.gitlab.com/ee/ci/)
* [Docker](https://www.docker.com/)
* [Keycloak](https://www.keycloak.org/)
* [Keycloak-Migration](https://mayope.github.io/keycloakmigration/)
* [PostgreSQL](https://www.postgresql.org/)
* [OWASP check](https://jeremylong.github.io/DependencyCheck/dependency-check-gradle/index.html)

## Getting started

### requirements 

* OpenJDK 17 or newer
* NodeJS 16.13.1
* Docker

### first steps

There are only a view steps for starting this application in development mode:

1. clone or download repository
2. run `./gradlew startLocalInfra` for linux or `.\gradlew startLocalInfra` for windows in the root folder of the project. 
3. run `./gradlew bootRun` for starting the backend service
4. go to frontend folder and run `npm install` for the installation of the frontend dependencies
5. run `npm start` for starting the application. Your browser should open [http://localhost:3000](http://localhost:3000)
6. login with username __writer__ and password __password__.
7. You should see the running application

For more information visit [the documentation folder of this project](documentation/index.md)

### testing project dockerized: 

1. go to the _e2e_infra_ folder 
2. run `docker compose up -d`
3. Visit [http://localhost:8080](http://localhost:8080)
4. login with username __writer__ and password __password__.

## contact

For questions or comments, please create an issue in this project. 
