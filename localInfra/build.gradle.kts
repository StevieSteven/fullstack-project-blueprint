
group = "net.stremo"
version = project.rootProject.version

tasks.register("startLocalInfra", Exec::class) {
    description = "Starts the local infrastructure"
    group = "infrastructure"
    commandLine = listOf("docker-compose", "up", "-d")
}
tasks.register("stopLocalInfra", Exec::class) {
    description = "Stops and removes local infrastructure from a docker environment."
    group = "infrastructure"
    commandLine = listOf("docker-compose", "down", "--rmi", "local", "-v")
}
