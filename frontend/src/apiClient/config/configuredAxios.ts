import axios, {AxiosHeaders, AxiosInterceptorManager, AxiosPromise, AxiosRequestConfig, AxiosResponse} from "axios";
import {AxiosDefaults, HeadersDefaults} from "axios";
type AxiosHeaderValue = AxiosHeaders | string | string[] | number | boolean | null;

export interface CustomAxiosInstance {
    (config: AxiosRequestConfig): AxiosPromise;
    (url: string, config?: AxiosRequestConfig): AxiosPromise;
    defaults: Omit<AxiosDefaults, "headers"> & {
        headers: HeadersDefaults & {
            [key: string]: AxiosHeaderValue
        }
    };
    interceptors: {
        request: AxiosInterceptorManager<AxiosRequestConfig>;
        response: AxiosInterceptorManager<AxiosResponse>;
    };
    getUri(config?: AxiosRequestConfig): string;
    request<T = any> (config: AxiosRequestConfig): Promise<T>;
    get<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
    delete<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
    head<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
    options<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
    post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
    put<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
    patch<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
}


const configuredAxios: CustomAxiosInstance = axios.create({
    baseURL: "/api",
});
configuredAxios.interceptors.response.use(
    (response: AxiosResponse<any>) => Promise.resolve<any>(response.data),
);
export default configuredAxios;