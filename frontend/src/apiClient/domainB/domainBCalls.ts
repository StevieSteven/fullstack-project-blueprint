import configuredAxios from "../config/configuredAxios";

export interface DomainBEntityResponse {
    readonly id: string;
    readonly name: string;
    readonly description?: string;
    readonly tags: string[];
}

export interface DomainBAddEntityRequest {
    readonly name: string;
    readonly description?: string;
    readonly tags: string[];
}
export const callGetDomainBEntities = () => configuredAxios.get<DomainBEntityResponse[]>("/domainB");
export const callGetDomainBEntry = (id: string) => configuredAxios.get<DomainBEntityResponse>(`/domainB/${id}`);

export const callAddDomainBEntry = (input: DomainBAddEntityRequest) => {
    const newEntry = {
        name: input.name,
        description: input.description,
        tags: input.tags
    }
    return configuredAxios.post<DomainBEntityResponse>("/domainB", newEntry);
};
