/*
* in this file, there should be the rest calls
 */

import {DomainAEntity} from "../../middleware/domainA/domainAQueries";
import configuredAxios from "../config/configuredAxios";

export const callGetDomainAEntities = () => configuredAxios.get<DomainAEntity[]>("/domainA");
