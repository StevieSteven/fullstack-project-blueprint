import {AppBar, Container, Menu, MenuItem, styled, Toolbar, Typography} from "@mui/material";
import {ReactNode, useState} from "react";
import {useTranslation} from "react-i18next";
import {useNavigate} from "react-router-dom";
import {keycloak} from "../../security/keycloakConfig";
import i18next from "i18next";
import {useUserInfo} from "../../security/useUserInfo";
import {Avatar} from "../../components/common/avatar/Avatar";

interface MainLayoutProps {
    readonly children: ReactNode;
}

export const MainLayout = (props: MainLayoutProps) => {
    const navigate = useNavigate();
    return (
        <div>
            <AppBar position={"static"}>
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{flexGrow: 1}} onClick={() => navigate("/")}>
                        React skeleton app
                    </Typography>
                    <AvatarDropdown/>
                </Toolbar>
            </AppBar>
            <Container>
                {props.children}
            </Container>
        </div>
    );
};
const AvatarDropdown = () => {
    const {t} = useTranslation();

    const logout = () => keycloak?.logout();
    const userInfo = useUserInfo().userInfo

    const onLanguageItemClick = (language: string) => {
        return () => {
            i18next.changeLanguage(language);
        };
    };

    const onAvatarClick = (e: any) => {
        e.preventDefault();
        setMenuOpen(true)
    };
    const [menuOpen, setMenuOpen] = useState<boolean>(false);

    const handleClose = () => setMenuOpen(false);

    return (
        <div id="avatar">
            <Avatar aria-controls="avatar-menu" aria-haspopup="true"
                          name={userInfo?.fullName ?? "anonymous user"} onClick={onAvatarClick}/>
            <Menu
                id="avatar-menu"
                anchorEl={document.getElementById("avatar")}
                keepMounted
                open={menuOpen}
                onClose={handleClose}
            >
                <MenuItem onClick={handleClose}><Bold>{userInfo?.username}</Bold></MenuItem>
                <MenuItem onClick={handleClose}><Bold>{userInfo?.email}</Bold></MenuItem>
                {
                    languages.map((language, index) => {
                        return (
                            <MenuItem key={index} onClick={onLanguageItemClick(language.code)}>
                                {language.label}
                            </MenuItem>
                        );
                    })
                }
                <MenuItem onClick={logout}>{t("logout")}</MenuItem>
            </Menu>
        </div>
    );
};

export const Bold = styled(Typography)({fontWeight: "bold"});

const languages = [
    {
        code: "en",
        label: "english"
    },
    {
        code: "es",
        label: "español"
    },
    {
        code: "de",
        label: "deutsch",
    },
    {
        code: "zh",
        label: "chinese"
    }
]