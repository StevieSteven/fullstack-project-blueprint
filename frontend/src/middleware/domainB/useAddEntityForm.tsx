import {useState} from "react";
import {useAddEntityMutation} from "./domainBQueries";

export interface CreateEntityError {
    readonly code: number;
    readonly field?: string;
    readonly validationCode?: string;
}
export interface AddEntityForm {
    readonly name: string;
    readonly setName: (name: string) => void;
    readonly description?: string;
    readonly setDescription: (description?: string) => void;
    readonly tags: string[];
    readonly setTags: (tags: string[]) => void;
    readonly submit: () => void;
    readonly error?: CreateEntityError;
}

export const useAddEntityForm = (): AddEntityForm => {
    const [name, setName] = useState<string>("");
    const [description, setDescription] = useState<string | undefined>();
    const [tags, setTags] = useState<string[]>([]);
    const mutation = useAddEntityMutation();

    const submit = () => {
        mutation.mutateAsync({
            name,
            description,
            tags
        }).then(() => {
            setName("");
            setDescription("");
            setTags([]);
        })
    }

    return {
        name,
        setName,
        description,
        setDescription,
        tags,
        setTags,
        submit,
        error: mutation.error || undefined
    };
};