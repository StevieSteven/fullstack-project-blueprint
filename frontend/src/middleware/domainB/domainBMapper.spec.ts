import {DomainBEntityResponse} from "../../apiClient/domainB/domainBCalls";
import {DomainBEntity} from "./domainBQueries";
import {mapDomainBEntityResponse} from "./domainBMapper";

describe("domainBMapper", () => {
    it("should map response correctly", () => {
        const response: DomainBEntityResponse = {
            id: "id",
            name:"name",
            description: "description",
            tags: ["tag1", "tag2"]
        }
        const result: DomainBEntity = {
            id: "id",
            name: "name",
            description: "description",
            tags: ["tag1", "tag2"]
        }
        expect(mapDomainBEntityResponse(response)).toEqual(result)
    });
});
