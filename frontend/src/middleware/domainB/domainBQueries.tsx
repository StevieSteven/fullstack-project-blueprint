import {useMutation, useQuery, useQueryClient} from "@tanstack/react-query";
import {callAddDomainBEntry, callGetDomainBEntities, callGetDomainBEntry} from "../../apiClient/domainB/domainBCalls";
import {mapDomainBEntityResponse} from "./domainBMapper";
import {CreateEntityError} from "./useAddEntityForm";

export interface DomainBEntity {
    readonly id: string;
    readonly name: string;
    readonly description?: string;
    readonly tags: string[];
}

export interface DomainBAddEntryInput {
    readonly name: string;
    readonly description?: string;
    readonly tags: string[];
}

const queryListSelector = "domain-B-list"
const queryDetailsSelector = "domain-B-details"

export const useGetEntities = () => useQuery<DomainBEntity[]>({
    queryKey: [queryListSelector],
    queryFn: () => callGetDomainBEntities()
        .then(data => Promise.resolve(data.map(it => mapDomainBEntityResponse(it))))
});

export const useGetEntity = (id: string) => useQuery<DomainBEntity>({
    queryKey: [queryDetailsSelector, id],
    queryFn: () => callGetDomainBEntry(id)
            .then(data => Promise.resolve(mapDomainBEntityResponse(data)))
    });

export const useAddEntityMutation = () => {
    const queryClient = useQueryClient();

    return useMutation<DomainBEntity, CreateEntityError, DomainBAddEntryInput>((input) => {
        return callAddDomainBEntry({
            name: input.name,
            description: input.description,
            tags: input.tags,
        }).then(data => Promise.resolve(mapDomainBEntityResponse(data)))
            .catch(e => {
                console.log("e", e.response.status)
                return Promise.reject({
                    code: e.response.status,
                    field: e.response.data.field,
                    validationCode: e.response.data.reason
                })
            });
    }, {
        onSuccess: (newData) => {
            queryClient.setQueriesData([queryListSelector], (oldData: DomainBEntity[] = []) => [...oldData, newData]);
        },
    });
};

