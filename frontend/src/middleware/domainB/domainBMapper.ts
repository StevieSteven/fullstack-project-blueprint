import {DomainBEntityResponse} from "../../apiClient/domainB/domainBCalls";
import {DomainBEntity} from "./domainBQueries";

export const mapDomainBEntityResponse = (response: DomainBEntityResponse): DomainBEntity => ({
    id: response.id,
    name: response.name,
    description: response.description,
    tags: response.tags
});