import {userInfo, UserInfo} from "./keycloakConfig";

interface UserInfoData {
    readonly loggedIn: boolean;
    readonly userInfo?: UserInfo
}

export const useUserInfo = (): UserInfoData => {
    const uI = userInfo || undefined;
    return {
        loggedIn: !!uI,
        userInfo: uI,
    };
};