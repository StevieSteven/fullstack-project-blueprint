import React, {Suspense} from "react"
import ReactDOM from "react-dom/client"
import App from "./App.tsx"
import "./index.css"
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import {ThemeProvider} from "@mui/material";
import {theme} from "./theming/theme";
import {I18nextProvider} from "react-i18next";
import i18n from "./i18n/i18n";
import {initKeycloak} from "./security/keycloakConfig";

initKeycloak();
const queryClient = new QueryClient();
ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
      <Suspense fallback={"loading"}>
          <I18nextProvider i18n={i18n}>
              <QueryClientProvider client={queryClient}>
                  <ThemeProvider theme={theme}>
                      <App/>
                  </ThemeProvider>
              </QueryClientProvider>
          </I18nextProvider>
      </Suspense>
  </React.StrictMode>,
)
