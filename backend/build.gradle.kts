import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jlleitschuh.gradle.ktlint.reporter.ReporterType
import java.net.URL
import de.klg71.keycloakmigrationplugin.KeycloakMigrationTask

plugins {
    id("org.springframework.boot") version "3.1.5"
    id("io.spring.dependency-management") version "1.1.3"
    kotlin("jvm") version "1.9.10"
    kotlin("plugin.spring") version "1.9.10"
    kotlin("plugin.jpa") version "1.9.10"
    id("org.jlleitschuh.gradle.ktlint") version "11.0.0"
    id("org.jlleitschuh.gradle.ktlint-idea") version "11.0.0"
    id("jacoco")
    id("org.owasp.dependencycheck") version "7.4.4" // "8.0.1" has a bug: https://github.com/jeremylong/DependencyCheck/issues/5313
    id("de.klg71.keycloakmigrationplugin") version "0.2.55"
}

java.sourceCompatibility = JavaVersion.VERSION_17
val backendVersion: String = project.version.toString()
println("backendVersion: $backendVersion")

apply(plugin = "org.owasp.dependencycheck")
apply(plugin = "org.jetbrains.kotlin.plugin.jpa")
apply(plugin = "org.jlleitschuh.gradle.ktlint")
apply(plugin = "de.klg71.keycloakmigrationplugin")

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("org.postgresql:postgresql:42.6.0")
    implementation("org.liquibase:liquibase-core:4.24.0")

    implementation("io.github.openfeign:feign-httpclient:13.0")
    implementation("io.github.openfeign:feign-jackson:13.0")

    implementation("org.springdoc:springdoc-openapi-starter-common:2.2.0")

    testImplementation("org.springframework.cloud:spring-cloud-starter-openfeign:4.0.4")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")

    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
}

noArg {
    invokeInitializers = true
}

ktlint {
    version.set("0.40.0") // 0.41.0 does not work
    debug.set(false)
    verbose.set(true)
    android.set(false)
    outputToConsole.set(true)
    ignoreFailures.set(false)
    disabledRules.set(setOf("no-wildcard-imports", "import-ordering"))
    reporters {
        reporter(ReporterType.PLAIN)
        reporter(ReporterType.CHECKSTYLE)
        reporter(ReporterType.HTML)
    }
    kotlinScriptAdditionalPaths {
        include(fileTree("scripts/"))
    }
    filter {
        exclude("**/generated/**")
        include("**/kotlin/**")
    }
}

jacoco {
    toolVersion = "0.8.7"
}

dependencyCheck {
    failOnError = true
    format = org.owasp.dependencycheck.reporting.ReportGenerator.Format.HTML
    analyzers.ossIndex.enabled = false
    analyzers.assemblyEnabled = false
    suppressionFile = "backend/owasp-ignore.xml"
}

springBoot {
    buildInfo {
        properties {
            artifact.set("fullstack-blueprint")
            group.set("de.stephanstrehler")
            name.set("fullstack blueprint")
            version.set(backendVersion)
        }
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}
tasks.test {
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}
tasks.withType<Test> {
    useJUnitPlatform()
    filter {
        includeTestsMatching("*Test")
    }
}

tasks.register<Copy>("copyFrontendFiles") {
    from("../frontend/dist")
    into("$buildDir/resources/main/static")
}

tasks.register("integration", Test::class) {
    description = "Runs the integration tests."
    group = "Verification"

    environment("spring.profiles.active", "it")
    filter {
        includeTestsMatching("*IT")
        excludeTestsMatching("*Test")
    }
    useJUnitPlatform()
    dependsOn(listOf("startITInfra"))
    mustRunAfter(listOf("startITInfra"))
    finalizedBy(listOf("stopITInfra"))
}

tasks.register("startITInfra", Exec::class) {
    description = "Starts the infrastructure of the integration tests"
    group = "infrastructure"
    workingDir("./src/test/resources/itInfra")
    commandLine = listOf("docker-compose", "up", "-d")
    finalizedBy("keycloakMigrateLocal")
}
tasks.register("stopITInfra", Exec::class) {
    description = "Stops and removes infrastructure of the integration tests from a docker environment."
    group = "infrastructure"
    workingDir("./src/test/resources/itInfra")
    commandLine = listOf("docker-compose", "down", "--rmi", "local", "-v")
}

tasks.register("gitlab-integration", Test::class) {
    description = "Runs the integration tests in gitlab."
    group = "Gitlab-Verification"
    doFirst {
        val url = environment["APP_USERCONTROL_BASEURL"] as String
        println("Waiting for $url ...")
        while (!isUp(url)) {
            Thread.sleep(1000)
        }
        println("base keycloak container is up")
    }
    useJUnitPlatform()
    filter {
        includeTestsMatching("*IT")
        excludeTestsMatching("*Test")
    }
}

tasks.register("keycloak-migration-in-gitlab", KeycloakMigrationTask::class) {
    migrationFile = "../keycloak-migrations/keycloak-changelog.yml"
    adminUser = "admin"
    adminPassword = "admin"
    baseUrl = "http://keycloak:8080"
    waitForKeycloak = true
    parameters = mapOf(
        "REALM_NAME" to "app-name",
        "CREATE_DUMMY_DATA" to "true"
    )
}

tasks.register("keycloakMigrateLocal", KeycloakMigrationTask::class) {
    migrationFile = "../keycloak-migrations/keycloak-changelog.yml"
    adminUser = "admin"
    adminPassword = "admin"
    baseUrl = "http://localhost:18181"
    waitForKeycloak = true
    parameters = mapOf(
        "REALM_NAME" to "app-name",
        "CREATE_DUMMY_DATA" to "true"
    )
}

fun isUp(url: String): Boolean {
    return try {
        URL(url).readBytes().isNotEmpty()
    } catch (error: Exception) {
        false
    }
}
