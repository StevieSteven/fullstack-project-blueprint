package de.stephanstrehler.fullstackprojectblueprintbackend.util

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import feign.Feign
import feign.form.FormEncoder
import feign.codec.Decoder
import feign.codec.Encoder
import feign.jackson.JacksonDecoder
import feign.jackson.JacksonEncoder

class KeycloakClientBuilder {
    fun getObjectMapper() = ObjectMapper()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .registerModule(KotlinModule.Builder().build())
        .registerModule(JavaTimeModule())

    fun getJacksonEncoder() = JacksonEncoder(getObjectMapper())
    fun getJacksonDecoder() = JacksonDecoder(getObjectMapper())

    inline fun <reified T> createStringRequestBodyClient(
        baseUrl: String,
        tokenUrl: String,
        user: String,
        password: String,
        realm: String,
        clientId: String,
    ): T {
        return createClient<T>(
            baseUrl = baseUrl,
            tokenUrl = tokenUrl,
            user = user,
            password = password,
            realm = realm,
            clientId = clientId,
            encoder = null,
            decoder = getJacksonDecoder()
        )
    }

    inline fun <reified T> createClient(
        baseUrl: String,
        tokenUrl: String,
        user: String,
        password: String,
        realm: String,
        clientId: String,
    ): T {
        return createClient<T>(
            baseUrl = baseUrl,
            tokenUrl = tokenUrl,
            user = user,
            password = password,
            realm = realm,
            clientId = clientId,
            encoder = getJacksonEncoder(),
            decoder = getJacksonDecoder()
        )
    }

    inline fun <reified T> createClient(
        baseUrl: String,
        tokenUrl: String,
        user: String,
        password: String,
        realm: String,
        clientId: String,
        encoder: Encoder?,
        decoder: Decoder?,
    ): T {
        return Feign.builder().run {
            val tokenHolder = TokenHolder(
                initKeycloakLoginClient(tokenUrl),
                user,
                password,
                realm,
                clientId
            )

            if (encoder != null) {
                encoder(encoder)
            }
            if (decoder != null) {
                decoder(decoder)
            }
            requestInterceptor {
                tokenHolder.token().run {
                    it.header("Authorization", "Bearer $accessToken")
                }
                it.header("Content-Type", "application/json")
            }
            target(T::class.java, baseUrl)
        }
    }

    fun initKeycloakLoginClient(
        baseUrl: String
    ): KeycloakLoginClient = Feign.builder().run {
        val objectMapper = getObjectMapper()
        encoder(FormEncoder(JacksonEncoder(objectMapper)))
        decoder(JacksonDecoder(objectMapper))

        target(KeycloakLoginClient::class.java, baseUrl)
    }
}
