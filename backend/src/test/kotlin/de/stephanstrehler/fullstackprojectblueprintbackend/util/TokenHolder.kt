package de.stephanstrehler.fullstackprojectblueprintbackend.util

import java.util.concurrent.TimeUnit

class TokenHolder(
    private val client: KeycloakLoginClient,
    private val user: String,
    private val password: String,
    private val realm: String,
    private val clientId: String,
) {

    private var token: AccessToken = client.login(realm, "password", clientId, user, password)
    private var tokenReceived: Long = System.currentTimeMillis()

    private fun tokenExpired() = System.currentTimeMillis() - tokenReceived > TimeUnit.SECONDS.toMillis(token.expiresIn!!)
    private fun refreshExpired() =
        System.currentTimeMillis() - tokenReceived > TimeUnit.SECONDS.toMillis(token.refreshExpiresIn!!)

    fun token(): AccessToken {
        if (tokenExpired()) {
            token = getNewToken()
            tokenReceived = System.currentTimeMillis()
        }
        return token
    }

    private fun getNewToken() = if (!refreshExpired()) {
        client.login(realm, "refresh_token", token.refreshToken!!, clientId)
    } else {
        client.login(realm, "password", clientId, user, password)
    }
}
