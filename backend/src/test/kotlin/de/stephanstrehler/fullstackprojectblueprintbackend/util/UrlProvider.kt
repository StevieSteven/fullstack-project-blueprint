package de.stephanstrehler.fullstackprojectblueprintbackend.util

class UrlProvider {
    private val defaultKeycloakUrl = "http://localhost:18181/"
    fun getKeycloakUrl(): String {
        return envOrDefault("APP_USERCONTROL_BASEURL", defaultKeycloakUrl)
    }

    private fun envOrDefault(envName: String, defaultValue: String): String {
        val envValue = System.getenv(envName)
        if (envValue != null) {
            return envValue
        }
        return defaultValue
    }
}
