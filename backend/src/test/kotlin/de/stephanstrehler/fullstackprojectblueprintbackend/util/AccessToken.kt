package de.stephanstrehler.fullstackprojectblueprintbackend.util

import com.fasterxml.jackson.annotation.JsonProperty

class AccessToken {
    @JsonProperty("refresh_expires_in")
    val refreshExpiresIn: Long? = null

    @JsonProperty("access_token")
    val accessToken: String? = null

    @JsonProperty("refresh_token")
    val refreshToken: String? = null

    @JsonProperty("expires_in")
    val expiresIn: Long? = null

    override fun toString(): String {
        return "Token(accessToken=$accessToken, refreshToken=$refreshToken, expiresIn=$expiresIn)"
    }
}
