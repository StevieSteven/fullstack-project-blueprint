package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB

import com.nhaarman.mockitokotlin2.whenever
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.mapper.DomainBMapper
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.db.DomainBEntity
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.dto.CreateDomainBInput
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.repositories.DomainBRepository
import de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.inputValidation.InputValidationException
import de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.inputValidation.InputValidationExceptionReason
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.mockito.Mockito.mock
import java.util.*

class DomainBServiceTest {

    private val domainARepository = mock(DomainBRepository::class.java)
    private val domainAElementMapper = DomainBMapper() // I don't mock mapper for better testing
    private val service = DomainBService(
        domainBRepository = domainARepository,
        domainBMapper = domainAElementMapper,
    )

    private val userId = UUID.randomUUID()

    @Test
    fun `should throw exception if name is blank`() {

        val exception = assertThrows<InputValidationException> {
            service.addElement(
                CreateDomainBInput(
                    name = " ",
                    description = null,
                    tags = emptyList()
                ),
                userId
            )
        }
        assertEquals("name", exception.field)
        assertEquals(InputValidationExceptionReason.MISSING, exception.reason)
    }

    @Test
    fun `should throw exception if name still exists`() {
        whenever(domainARepository.existsByName("name")).thenReturn(true)

        val exception = assertThrows<InputValidationException> {
            service.addElement(
                CreateDomainBInput(
                    name = "name",
                    description = null,
                    tags = emptyList()
                ),
                userId
            )
        }
        assertEquals("name", exception.field)
        assertEquals(InputValidationExceptionReason.STILL_EXISTS, exception.reason)
    }

    @Test
    fun `should create element if name is valid`() {
        whenever(domainARepository.existsByName("name")).thenReturn(false)
        whenever(domainARepository.save(Mockito.any(DomainBEntity::class.java))).thenAnswer {
            it.arguments[0]
        }

        val result =
            service.addElement(
                CreateDomainBInput(
                    name = "name",
                    description = null,
                    tags = emptyList(),
                ),
                userId
            )
        assertEquals("name", result.name)
        assertEquals(userId, result.createdBy)
    }
}
