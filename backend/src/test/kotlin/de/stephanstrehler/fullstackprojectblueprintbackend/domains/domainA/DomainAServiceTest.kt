package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA

import com.nhaarman.mockitokotlin2.whenever
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.mapper.DomainAMapper
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.db.DomainAEntity
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.dto.DomainAInput
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.repositories.DomainARepository
import de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.inputValidation.InputValidationException
import de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.inputValidation.InputValidationExceptionReason
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.mockito.Mockito.mock
import java.util.*

class DomainAServiceTest {

    private val domainARepository = mock(DomainARepository::class.java)
    private val domainAMapper = DomainAMapper() // I don't mock mapper for better testing
    private val service = DomainAService(
        domainARepository = domainARepository,
        domainAMapper = domainAMapper,
    )

    private val userId = UUID.randomUUID()

    @Test
    fun `should throw exception if name is blank`() {

        val exception = assertThrows<InputValidationException> {
            service.addElement(
                DomainAInput(
                    name = " "
                ),
                userId
            )
        }
        assertEquals("name", exception.field)
        assertEquals(InputValidationExceptionReason.MISSING, exception.reason)
    }

    @Test
    fun `should throw exception if name still exists`() {
        whenever(domainARepository.existsByName("name")).thenReturn(true)

        val exception = assertThrows<InputValidationException> {
            service.addElement(
                DomainAInput(
                    name = "name"
                ),
                userId
            )
        }
        assertEquals("name", exception.field)
        assertEquals(InputValidationExceptionReason.STILL_EXISTS, exception.reason)
    }

    @Test
    fun `should create element if name is valid`() {
        whenever(domainARepository.existsByName("name")).thenReturn(false)
        whenever(domainARepository.save(Mockito.any(DomainAEntity::class.java))).thenAnswer {
            it.arguments[0]
        }

        val result =
            service.addElement(
                DomainAInput(
                    name = "name"
                ),
                userId
            )
        assertEquals("name", result.name)
        assertEquals(userId, result.createdBy)
    }
}
