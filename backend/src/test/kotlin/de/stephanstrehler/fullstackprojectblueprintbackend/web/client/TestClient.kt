package de.stephanstrehler.fullstackprojectblueprintbackend.web.client

import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.dto.DomainADTO
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.dto.DomainBDTO
import de.stephanstrehler.fullstackprojectblueprintbackend.web.model.AddDomainAElementRequestBody
import de.stephanstrehler.fullstackprojectblueprintbackend.web.model.AddDomainBElementRequestBody
import feign.Param
import feign.RequestLine
import org.springframework.web.bind.annotation.RequestBody
import java.util.*

interface TestClient {

    @RequestLine("GET /domainA")
    fun getAllDomainAElements(): List<DomainADTO>

    @RequestLine("GET /domainA/{element-id}")
    fun getDomainAElement(@Param("element-id") elementId: UUID): DomainADTO

    @RequestLine("POST /domainA")
    fun addDomainAElement(@RequestBody input: AddDomainAElementRequestBody): DomainADTO

    @RequestLine("GET /domainB")
    fun getAllDomainBElements(): List<DomainBDTO>

    @RequestLine("GET /domainB/{element-id}")
    fun getDomainBElement(@Param("element-id") elementId: UUID): DomainBDTO

    @RequestLine("POST /domainB")
    fun addDomainBElement(@RequestBody input: AddDomainBElementRequestBody): DomainBDTO
}
