package de.stephanstrehler.fullstackprojectblueprintbackend.web

import de.stephanstrehler.fullstackprojectblueprintbackend.web.client.TestClient
import de.stephanstrehler.fullstackprojectblueprintbackend.web.client.createReaderTestClient
import de.stephanstrehler.fullstackprojectblueprintbackend.web.client.createWriterTestClient
import de.stephanstrehler.fullstackprojectblueprintbackend.web.model.AddDomainBElementRequestBody
import feign.FeignException
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import java.util.*

/**
 * Example for writing integration tests using feign client. Tests are not complete!
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("it")
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@DirtiesContext
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DomainBRestControllerIT {
    @LocalServerPort
    var port: Int? = null

    private lateinit var writerTestClient: TestClient
    private lateinit var readerTestClient: TestClient

    private var createdId: UUID? = null

    @BeforeAll
    fun createClients() {
        writerTestClient = createWriterTestClient(port!!)
        readerTestClient = createReaderTestClient(port!!)
    }

    @Order(1)
    @Test
    fun `should create entity with correct input data and writer user`() {
        val result = writerTestClient.addDomainBElement(
            AddDomainBElementRequestBody(
                name = "name",
                description = "description",
                tags = listOf("orange", "blue")
            )
        )
        createdId = result.id

        assertEquals("name", result.name)
        assertEquals("description", result.description)
        assertEquals(listOf("orange", "blue"), result.tags)
    }

    @Order(2)
    @Test
    fun `should return element with given id`() {
        val writerResult = writerTestClient.getDomainBElement(createdId!!)
        assertEquals("name", writerResult.name)
        assertEquals("description", writerResult.description)
        assertEquals(listOf("orange", "blue"), writerResult.tags)

        val readerResult = readerTestClient.getDomainBElement(createdId!!)
        assertEquals("name", readerResult.name)
        assertEquals("description", readerResult.description)
        assertEquals(listOf("orange", "blue"), readerResult.tags)
    }

    @Order(3)
    @Test
    fun `should throw exception if reader wants to create element`() {
        val exception = assertThrows<FeignException> {
            readerTestClient.addDomainBElement(
                AddDomainBElementRequestBody(
                    name = "name",
                    description = "description",
                    tags = listOf("orange", "blue")
                )
            )
        }

        assertEquals(403, exception.status())
    }
}
