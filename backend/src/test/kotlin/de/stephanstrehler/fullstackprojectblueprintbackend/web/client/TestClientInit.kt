package de.stephanstrehler.fullstackprojectblueprintbackend.web.client

import de.stephanstrehler.fullstackprojectblueprintbackend.util.KeycloakClientBuilder
import de.stephanstrehler.fullstackprojectblueprintbackend.util.UrlProvider

fun createWriterTestClient(port: Int) = createTestClient(port, "writer", "password")
fun createReaderTestClient(port: Int) = createTestClient(port, "reader", "password")
fun createTestClientForStringRequests(port: Int) = createTestClientForStringRequests(port, "writer", "password")

fun createTestClient(port: Int, user: String, password: String): TestClient = KeycloakClientBuilder().createClient(
    baseUrl = "http://localhost:$port/api",
    tokenUrl = UrlProvider().getKeycloakUrl(),
    clientId = "app-frontend",
    user = user,
    password = password,
    realm = "app-name"
)
fun createTestClientForStringRequests(port: Int, user: String, password: String): TestClient = KeycloakClientBuilder().createStringRequestBodyClient(
    baseUrl = "http://localhost:$port/api",
    tokenUrl = UrlProvider().getKeycloakUrl(),
    clientId = "app-frontend",
    user = user,
    password = password,
    realm = "app-name",
)
