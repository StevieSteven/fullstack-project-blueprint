package de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.inputValidation

enum class InputValidationExceptionReason {
    MISSING,
    WRONG_FORMAT,
    STILL_EXISTS,
    SHOULD_BE_NULL,
    DOES_NOT_EXIST
}
