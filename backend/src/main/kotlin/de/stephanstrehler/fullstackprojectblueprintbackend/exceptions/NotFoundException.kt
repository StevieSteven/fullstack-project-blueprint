package de.stephanstrehler.fullstackprojectblueprintbackend.exceptions

import org.springframework.http.HttpStatus
import java.util.*

class NotFoundException(
    val id: String,
    val clazz: Class<*>
) : HTTPExceptionResponse(
    HttpStatus.NOT_FOUND,
    mapOf(
        "message" to "object does not exist",
        "translationCode" to "notFoundError",
        "class" to clazz.simpleName,
        "id" to id,
    )
) {
    constructor(uuid: UUID, clazz: Class<*>) : this(uuid.toString(), clazz)
}
