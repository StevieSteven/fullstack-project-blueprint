package de.stephanstrehler.fullstackprojectblueprintbackend.exceptions

import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class CustomRestExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [HTTPExceptionResponse::class])
    fun handleCustomException(ex: HTTPExceptionResponse, request: WebRequest): ResponseEntity<Any>? {
        val headers = HttpHeaders()
        return handleExceptionInternal(ex, ex.data, headers, ex.status, request)
    }
}
