package de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.inputValidation

import de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.HTTPExceptionResponse
import org.springframework.http.HttpStatus

class InputValidationException(
    val clazz: Class<*>,
    val field: String,
    val reason: InputValidationExceptionReason = InputValidationExceptionReason.MISSING
) : HTTPExceptionResponse(
    HttpStatus.BAD_REQUEST,
    mapOf(
        "message" to "input validation error",
        "translationCode" to "inputValidationError",
        "class" to clazz.simpleName,
        "field" to field,
        "reason" to reason.toString()
    )
)
