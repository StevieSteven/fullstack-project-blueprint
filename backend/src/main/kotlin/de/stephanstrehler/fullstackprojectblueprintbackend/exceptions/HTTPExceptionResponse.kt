package de.stephanstrehler.fullstackprojectblueprintbackend.exceptions

import org.springframework.http.HttpStatus

open class HTTPExceptionResponse(val status: HttpStatus, val data: Map<String, String>) : RuntimeException()
