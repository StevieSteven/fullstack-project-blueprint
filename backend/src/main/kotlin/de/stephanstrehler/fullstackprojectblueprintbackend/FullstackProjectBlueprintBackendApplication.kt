package de.stephanstrehler.fullstackprojectblueprintbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FullstackProjectBlueprintBackendApplication

fun main(args: Array<String>) {
    runApplication<FullstackProjectBlueprintBackendApplication>(*args)
}
