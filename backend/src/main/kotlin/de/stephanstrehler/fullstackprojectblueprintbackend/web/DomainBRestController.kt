package de.stephanstrehler.fullstackprojectblueprintbackend.web

import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.DomainBService
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.dto.CreateDomainBInput
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.dto.DomainBDTO
import de.stephanstrehler.fullstackprojectblueprintbackend.security.AuthN
import de.stephanstrehler.fullstackprojectblueprintbackend.web.model.AddDomainBElementRequestBody
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("\${app.apiBasePath}/domainB")
class DomainBRestController(
    val domainBService: DomainBService,
    val authN: AuthN
) {

    @GetMapping
    fun getElements() = domainBService.getAllElements()

    @GetMapping("/{id}")
    fun getElement(@PathVariable id: UUID): DomainBDTO {
        return domainBService.getElement(id)
    }

    @PostMapping
    @PreAuthorize("@authN.hasRole(\"WRITE\")")
    fun addElement(@RequestBody body: AddDomainBElementRequestBody): DomainBDTO {
        val userId = authN.getIDFromCurrentUser()
        return domainBService.addElement(CreateDomainBInput(name = body.name, description = body.description, tags = body.tags), userId)
    }
}
