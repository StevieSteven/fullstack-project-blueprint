package de.stephanstrehler.fullstackprojectblueprintbackend.web.model

data class AddDomainBElementRequestBody(
    val name: String,
    val description: String?,
    val tags: List<String>
)
