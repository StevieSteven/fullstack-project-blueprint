package de.stephanstrehler.fullstackprojectblueprintbackend.web.model

data class AddDomainAElementRequestBody(
    val name: String
)
