package de.stephanstrehler.fullstackprojectblueprintbackend.web

import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.DomainAService
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.dto.DomainAInput
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.dto.DomainADTO
import de.stephanstrehler.fullstackprojectblueprintbackend.security.AuthN
import de.stephanstrehler.fullstackprojectblueprintbackend.web.model.AddDomainAElementRequestBody
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("\${app.apiBasePath}/domainA")
class DomainARestController(
    val domainAService: DomainAService,
    val authN: AuthN
) {

    @GetMapping
    fun getElements() = domainAService.getAllElements()

    @GetMapping("/{id}")
    fun getElement(@RequestParam("id") id: UUID) = domainAService.getElement(id)

    @PostMapping
    fun addElement(@RequestBody body: AddDomainAElementRequestBody): DomainADTO {
        val userId = authN.getIDFromCurrentUser()

        return domainAService.addElement(DomainAInput(name = body.name), userId)
    }
}
