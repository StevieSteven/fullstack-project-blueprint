package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.dto

import java.time.LocalDateTime
import java.util.UUID

data class DomainBDTO(
    val id: UUID,
    val name: String,
    val description: String?,
    val tags: List<String>,
    val createdAt: LocalDateTime,
    val createdBy: UUID
)
