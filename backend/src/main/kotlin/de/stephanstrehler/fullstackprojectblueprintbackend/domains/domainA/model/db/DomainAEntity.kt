package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.db

import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.db.DomainBEntity
import jakarta.persistence.*
import java.time.LocalDateTime
import java.util.*

@Entity
@Table(name = "domain_a")
data class DomainAEntity(
    @Id
    val id: UUID,
    val name: String,
    val createdAt: LocalDateTime,
    val createdBy: UUID
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DomainBEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (createdAt != other.createdAt) return false
        if (createdBy != other.createdBy) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + createdAt.hashCode()
        result = 31 * result + createdBy.hashCode()
        return result
    }
}
