package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.db

import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.TagColumnConverter
import jakarta.persistence.*
import java.time.LocalDateTime
import java.util.*

@Entity
@Table(name = "domain_b")
data class DomainBEntity(
    @Id
    val id: UUID,
    val name: String,
    val description: String?,
    @Convert(converter = TagColumnConverter::class)
    val tags: List<String>,
    val createdAt: LocalDateTime,
    val createdBy: UUID
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DomainBEntity

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (tags != other.tags) return false
        if (createdAt != other.createdAt) return false
        if (createdBy != other.createdBy) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + tags.hashCode()
        result = 31 * result + createdAt.hashCode()
        result = 31 * result + createdBy.hashCode()
        return result
    }
}
