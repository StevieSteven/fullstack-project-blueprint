package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA

import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.mapper.DomainAMapper
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.db.DomainAEntity
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.dto.DomainAInput
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.dto.DomainADTO
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.repositories.DomainARepository
import de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.inputValidation.InputValidationException
import de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.inputValidation.InputValidationExceptionReason
import jakarta.annotation.PostConstruct
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*

@Service
class DomainAService(
    private val domainARepository: DomainARepository,
    private val domainAMapper: DomainAMapper
) {

    fun addElement(input: DomainAInput, userId: UUID): DomainADTO {
        if (input.name.isBlank()) {
            throw InputValidationException(
                clazz = DomainADTO::class.java,
                field = "name",
                reason = InputValidationExceptionReason.MISSING
            )
        }
        if (domainARepository.existsByName(input.name)) {
            throw InputValidationException(
                clazz = DomainADTO::class.java,
                field = "name",
                reason = InputValidationExceptionReason.STILL_EXISTS
            )
        }
        val element = DomainAEntity(
            id = UUID.randomUUID(),
            name = input.name,
            createdAt = LocalDateTime.now(),
            createdBy = userId
        )
        return domainARepository.save(element).let { domainAMapper.toDTO(it) }
    }

    fun getAllElements() = domainARepository.findAll().map { domainAMapper.toDTO(it) }

    fun getElement(id: UUID) = domainARepository.findByIdOrNull(id)?.let { domainAMapper.toDTO(it) }

    @PostConstruct
    fun createDummyData() {
        val userId = UUID.randomUUID()
        domainARepository.deleteAll()
        domainARepository.saveAll(
            listOf(
                DomainAEntity(
                    id = UUID.randomUUID(),
                    name = "first entity",
                    createdAt = LocalDateTime.now(),
                    createdBy = userId
                ),
                DomainAEntity(
                    id = UUID.randomUUID(),
                    name = "second entity",
                    createdAt = LocalDateTime.now(),
                    createdBy = userId
                ),
                DomainAEntity(
                    id = UUID.randomUUID(),
                    name = "third entity",
                    createdAt = LocalDateTime.now(),
                    createdBy = userId
                )
            )
        )
    }
}
