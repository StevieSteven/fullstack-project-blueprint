package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.dto

data class DomainAInput(
    val name: String
)
