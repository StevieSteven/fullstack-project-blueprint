package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.mapper

import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.db.DomainBEntity
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.dto.DomainBDTO
import org.springframework.stereotype.Component
import java.util.*

@Component
class DomainBMapper {

    fun toDTO(entity: DomainBEntity) = DomainBDTO(
        id = entity.id,
        name = entity.name,
        description = entity.description,
        tags = entity.tags,
        createdAt = entity.createdAt,
        createdBy = entity.createdBy
    )
}
