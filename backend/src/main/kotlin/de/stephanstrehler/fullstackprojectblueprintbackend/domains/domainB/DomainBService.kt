package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB

import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.mapper.DomainBMapper
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.dto.DomainBDTO
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.db.DomainBEntity
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.dto.CreateDomainBInput
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.repositories.DomainBRepository
import de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.NotFoundException
import de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.inputValidation.InputValidationException
import de.stephanstrehler.fullstackprojectblueprintbackend.exceptions.inputValidation.InputValidationExceptionReason
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*

@Service
class DomainBService(
    private val domainBRepository: DomainBRepository,
    private val domainBMapper: DomainBMapper
) {

    fun addElement(input: CreateDomainBInput, userId: UUID): DomainBDTO {
        if (input.name.isBlank()) {
            throw InputValidationException(
                clazz = DomainBDTO::class.java,
                field = "name",
                reason = InputValidationExceptionReason.MISSING
            )
        }
        if (domainBRepository.existsByName(input.name)) {
            throw InputValidationException(
                clazz = DomainBDTO::class.java,
                field = "name",
                reason = InputValidationExceptionReason.STILL_EXISTS
            )
        }
        val element = DomainBEntity(
            id = UUID.randomUUID(),
            name = input.name,
            tags = input.tags,
            description = input.description,
            createdAt = LocalDateTime.now(),
            createdBy = userId
        )
        return domainBRepository.save(element).let { domainBMapper.toDTO(it) }
    }

    fun getAllElements() = domainBRepository.findAll().map { domainBMapper.toDTO(it) }

    fun getElement(id: UUID) =
        domainBRepository.findByIdOrNull(id)?.let { domainBMapper.toDTO(it) } ?: throw NotFoundException(
            id,
            DomainBDTO::class.java
        )
}
