package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.repositories

import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.db.DomainAEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface DomainARepository : JpaRepository<DomainAEntity, UUID> {
    fun existsByName(name: String): Boolean
}
