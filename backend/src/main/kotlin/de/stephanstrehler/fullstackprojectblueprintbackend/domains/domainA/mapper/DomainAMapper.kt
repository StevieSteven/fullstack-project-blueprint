package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.mapper

import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.db.DomainAEntity
import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.dto.DomainADTO
import org.springframework.stereotype.Component

@Component
class DomainAMapper {

    fun toDTO(entity: DomainAEntity) = DomainADTO(
        id = entity.id,
        name = entity.name,
        createdAt = entity.createdAt,
        createdBy = entity.createdBy
    )
}
