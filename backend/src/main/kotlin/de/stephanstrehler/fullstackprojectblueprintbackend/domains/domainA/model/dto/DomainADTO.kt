package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainA.model.dto

import java.time.LocalDateTime
import java.util.UUID

data class DomainADTO(
    val id: UUID,
    val name: String,
    val createdAt: LocalDateTime,
    val createdBy: UUID
)
