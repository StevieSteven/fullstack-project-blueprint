package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.repositories

import de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.db.DomainBEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface DomainBRepository : JpaRepository<DomainBEntity, UUID> {
    fun existsByName(name: String): Boolean
}
