package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model.dto

data class CreateDomainBInput(
    val name: String,
    val description: String?,
    val tags: List<String>
)
