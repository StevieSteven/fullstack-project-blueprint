package de.stephanstrehler.fullstackprojectblueprintbackend.domains.domainB.model

import com.fasterxml.jackson.databind.ObjectMapper
import jakarta.persistence.AttributeConverter
import org.springframework.stereotype.Component

@Component
class TagColumnConverter(
    private val objectMapper: ObjectMapper
) : AttributeConverter<List<String>, String?> {
    override fun convertToDatabaseColumn(attribute: List<String>?): String? {
        return objectMapper.writeValueAsString(attribute)
    }

    override fun convertToEntityAttribute(dbData: String?): List<String> {
        return objectMapper.readValue(
            dbData,
            objectMapper.typeFactory.constructCollectionType(List::class.java, String::class.java)
        )
    }
}
