package de.stephanstrehler.fullstackprojectblueprintbackend.security

data class AppUserDetails(
    val id: String,
    val username: String,
    val surname: String,
    val forename: String,
    val email: String,
    val groups: List<String> = emptyList(),
    val roles: List<String> = emptyList()
)
