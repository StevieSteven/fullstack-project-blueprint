package de.stephanstrehler.fullstackprojectblueprintbackend.security

import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Component

@Component
class AppUserDetailsDeserializer {

    fun parse(jwt: Jwt): AppUserDetails {
        val claims: Map<String, Any> = jwt.claims

        return AppUserDetails(
            id = claimToString(claims, "sub"),
            username = claimToString(claims, "username"),
            forename = claimToString(claims, "given_name"),
            email = claimToString(claims, "email"),
            groups = claimToList(claims, "groups"),
            roles = getRoles(claims),
            surname = claimToString(claims, "family_name")
        )
    }

    private fun claimToString(claim: Map<String, Any>, key: String) = (claim[key] ?: "") as String

    private fun claimToList(claim: Map<String, Any>, key: String): List<String> {
        if (claim[key] !is List<*>) {
            return emptyList()
        }
        @Suppress("UNCHECKED_CAST")
        return (claim[key] ?: emptyList<String>()) as List<String>
    }

    private fun getRoles(claim: Map<String, Any>): List<String> {
        val realmAccess = claim["realm_access"] as Map<*, *>? ?: return emptyList()
        @Suppress("UNCHECKED_CAST")
        return (realmAccess["roles"] as List<*>? ?: emptyList<String>()) as List<String>
    }
}
