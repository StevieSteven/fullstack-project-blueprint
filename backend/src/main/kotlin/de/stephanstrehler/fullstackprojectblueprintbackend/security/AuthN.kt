package de.stephanstrehler.fullstackprojectblueprintbackend.security

import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import java.util.*

@Component
class AuthN {

    fun getCurrentLoggedInUser(): AppUserDetails {
        return getPossiblyLoggedInUser() ?: throw RuntimeException("could not get current user")
    }

    fun getIDFromCurrentUser(): UUID {
        return UUID.fromString(getCurrentLoggedInUser().id)
    }

    fun hasRole(role: String): Boolean {
        return getCurrentLoggedInUser().roles.contains(role)
    }
}

fun getPossiblyLoggedInUser(): AppUserDetails? {
    val authN = getAuthentication() ?: return null

    val principal = authN.principal
    if (principal !is Jwt) {
        throw RuntimeException("principal is not a JWT Token")
    }

    return AppUserDetailsDeserializer().parse(principal)
}

fun getAuthentication(): Authentication? {
    return SecurityContextHolder.getContext().authentication
}
