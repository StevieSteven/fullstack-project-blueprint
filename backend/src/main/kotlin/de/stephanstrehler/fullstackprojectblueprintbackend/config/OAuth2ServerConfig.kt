package de.stephanstrehler.fullstackprojectblueprintbackend.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder
import org.springframework.security.web.SecurityFilterChain

@Configuration
@EnableMethodSecurity(prePostEnabled = true)
class OAuth2ServerConfig {

    @Value("\${app.apiBasePath}")
    private lateinit var apiBasePath: String

    private val permittedUrls = listOf<String>("/error", "/actuator")

    @Bean
    @Throws(Exception::class)
    fun filterChain(
        http: HttpSecurity,
    ): SecurityFilterChain? {
        http
            .csrf {
                it.ignoringRequestMatchers(*permittedUrls.toTypedArray()).disable()
            }
            .authorizeHttpRequests {
                it.requestMatchers("$apiBasePath/**")
                    .fullyAuthenticated()
                    .requestMatchers("/**")
                    .permitAll()
            }
            .oauth2ResourceServer {
                it.jwt {
                }
            }
        return http.build()
    }

    @Bean
    fun jwtDecoder(properties: OAuth2ResourceServerProperties): JwtDecoder? =
        NimbusJwtDecoder.withJwkSetUri(properties.jwt.jwkSetUri).build()
}
