# Documentation of the fullstack project

In the following chapters you can find the information about the different software parts. It is only an overview about all used technologies. For detailed information visit the linked websites or spend time to look to the source code. If some information is missing, please open an issue. 

## main idea

The main idea of the project is to provide an easy and understandable way for starting a fullstack software project as junior developer or computer science student. 
It should be an entry for getting the first experiences in building, testing and hosting an application. Advanced topics and improvements like independent deployments of frontend and backend is not part of this project. 

All used technologies are scalable and maintained. They can be transformed into parts in a bigger architecture. 

The project is not in a final version. If you have got feedback or questions, please let me know and open an issue. Thank you very much.

## General architecture

First of all we should have a look to main architecture of project.

Generally, there are four components in the application: 
* frontend, written in React
* backend, written in Spring Boot and Kotlin
* PostgreSQL as database
* Keycloak as identity provider 


![General architecture](images/general-architecture.png)

After the build and release all parts run in a Docker Compose project. The frontend files are provided by the backend. So the user calls the backend url and gets the frontend files.

## backend

The backend is built with Kotlin as programming language and Spring Boot as web server framework. It uses Liquibase for database migrations.

### Kotlin and Gradle 
Kotlin is an JVM compatible programming language maintained by JetBrains (the company behind Idea and other IDEs). Kotlin adds some handy features like null pointer safety, optional chaining, data classes and a better codestyle in the usage of streams (my opinion). 

Gradle is a build tool like Maven. With Gradle you can manage your dependencies and can add some build scripts. In this project Kotlin Script is used for the work with Gradle.

> With the following links you can find the 
> 
> [Kotlin Documentation](https://kotlinlang.org/) 
>
> [Gradle Documentation](https://gradle.org/) 


### Spring Boot

The backend runs with the Spring Boot framework. Spring Boot provides us features like RESTful Service Controller or an easy way to connecting our backend with a database. A Spring Boot project can be setup easily with tht [Spring Initializer tool](https://start.spring.io/).

#### main structure of the backend

Basically there are three layers in the backend: 
1. REST Controller layer 
2. Service layer
3. Repository layer

Each layer has his own issues.

![General backend structure](images/backend.png)

##### REST Controller layer

The REST controllers provide the adapters to the HTTP protocol. They listen to the given HTTP path and parsing the input stream to our Java / Kotlin objects. 
When configured, the authorisation check is triggered and check the authorisation. 
If the given user has the permissions to execute the action, the REST controller calls the service. 
The result of the service is serialized and returned as stream. 
A REST controller should not include any business logic.

##### Service layer 

The service layer is the place for the business logic. 

That means there is an input validation when adding data.
The service layer calls the repository to getting the data from database. 
After that, the entities will be mapped to DTOs (data transfer objects). A short explanation is provided by [Baeldung](https://www.baeldung.com/entity-to-and-from-dto-for-a-java-spring-application).

##### Repository layer

The repository layer is used to create SQL queries easily and independent of the different SQL databases (PostgresSQL, Oracle, MySql, ...). Normally you should use the _JPA Repository_, because it is the most powerful repository.

#### Provide frontend after built

Spring Boot provides us the possibility to managing static files as resource. The short explanation for that is, that every file you put in the _static_, _public_, _resources_ or _META_INF/resources_ folder is available for the client. In our case we put the built frontend into the static folder. So the client can access the _index.html_ via _http://localhost:8080/index.html_. For more information you can visit [Baeldung](https://www.baeldung.com/spring-mvc-static-resources).

The details of providing our frontend with our SpringBoot application is documented in the _infrastructure and deployment_ chapter. 

### Liquibase

Everytime we work with databases we need to define the database structure at first. From time to time the database schema has to be increased or updated. For that we are using Liquibase. Everytime we start your backend, Liquibase ist checking the current database structure and compare it to our database migration files. The migration files are located in _backend/src/main/resources/db.changelog_. 

The main benefit of Liquibase is that it is (nearly) independent of the SQL language (e.g. PostgreSQL, Oracle, MSSQL and so on). As developers, we only write our migrations in the yaml files. For special cases we can also embed SQL files directly.

More information are provided by [Liquibase](https://www.liquibase.org/) itself.

## frontend

The SPA-frontend is built with [React](https://reactjs.org/). React is a Single Page Application framework which is maintained by Meta.

In generell I would suggest you to use a "three layer architecture" (the quotes means, that is in my opinion not a real architecture).

### main architecture

For every project, one of the main goals is to be independent to the neighbor systems. In case of the frontend there are to "systems" we have to interact with. 

The first is the backend, which provides us information over the API. Over the time, the API can change and we have to adapt our frontend.


The second "system" is the user of the application. The result of UX research or user feedback can be, that we have to change parts of our application. 

In both situation, we do not want to change the complete code of the frontend. For that we create the "three layer architecture" shown in the next image.

![General frontend structure](images/frontend.png)

The key points of the image are: 

* If you have multiple domains you should split them strictly. Each domain has its own UI, middleware and api layer. So it would be easier if you want to move a domain to a separate application later. Also the source code is easier to understand for new colleagues. 
* The UI layer and the API layer should not know each other. (In simple domains I still use the response objects directly in the UI. You can extract it, when the domain grows.)
* The Api client should be completely independent of the SPA framework itself. Also, it should not contain any i18n logic like translations.
* The UI layer should not include complex business logic, because it makes testing harder than needed. Personally, I extract business logic to pure TypeScript functions. So I can easily write tests.
* Try to reduce SPA specific code in your middleware. So a possible switch between two technologies would be possible. Hard but just possibly. 

### React

For setting up a new frontend application you can use the [create-react-app](https://create-react-app.dev/) package. Only for notice, there are alternative ways like using vite. 

When the app is created, there are a couple of .tsx files. In this files, [JSX](https://react.dev/learn/writing-markup-with-jsx) is used. That means the files include a Markup language which is a kind of HTML language extension. 

### TypeScript

TypeScript is maintained by Microsoft. It provides types in will be compiled to JavaScript. It is 100% compatible to JavaScript. 
The types are only available in development time. Because TypeScript is compiled to JavaScript it is neither faster nor slower than JavaScript. There is a good starting point for JavaScript developers for learning TypeScript provided by [typescriptlang.org](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html).

After mastering your first steps you can go further and use more advanced features like [Generics](https://www.typescriptlang.org/docs/handbook/2/generics.html).

### TanStack Query

[TanStack Query](https://tanstack.com/query/latest) is a middleware frontend framework. Next to React, you can use it for Solid, Vue and Svelte too. 
TanStack Query provides us a cache and state management of all data fetches and network requests. In the frontend sources of this project you can find _src/middleware_ folder. 

For a short explanation how TanStack Query works, we do a look to the DomainB middleware (path of file: _frontend/src/middleware/domainB/domainBQueries.tsx_).

First of all you can see that there are two different hooks. 

1. useQuery
2. useMutation

The _useQuery_ hook is used to get data from the data source. It haven't got any side effects. Each query has got an own query key which is like an unique identifier. Next to the query key there is the query function. It is executed when the hook is called and the result of itself is not valid anymore (e.g. after the caching time is over or the query client that the state set the state to invalid.)
The useQuery hook returns an object with has got a collection of properties. The most important ones are: 
* __data__: The result of the query function (could be undefined)
* __isLoading__: boolean which shows us if the query is loading
* __isError__: boolean which shows us if there was an error while executing the query function
* __error__: if there is an error while executing the query function, the error is returned. Could be undefined if the query function was finished successfully. 

The _useMutation_ hook is used to manipulate data (eg. adding, updating or deleting data entries). So everytime we trigger an action with side effects we have to use the useMutation hook.
In the middleware of domain B there is a custom hook called _useAddEntityMutation_. This is a function which is handling the creation of a new entry. 
The use useMutation hook need a mutation function. The mutation function can have a parameter list. In the mutation function the update logic (e.g. the network request) can be executed. 
The second parameter of the useMutation hook is the option object. In this we can define a callback function which is triggered every time the mutation function is finished successfully. 
In the case of _useAddEntityMutation_ we update the data of the query data of the _useGetEntities_ query hook. This will trigger a rerender of our entity list and we will the the added entity automatically. 

The most important properties of the returned object of useMutation:
* mutate / mutateAsync: for executing the mutation
* all properties described in useQuery

### Mui

[Material UI](https://mui.com/) is a component library for React based on the [Material Design](https://m3.material.io/) style guide of Google. In this project it is mainly used for providing the general components like Boxes, Cards and Typographies. 

### i18next

For the possibility to deliver the app with different languages we are using [i18next](https://www.i18next.com/). This is a SPA framework independent internationalization framework which provides different kinds of adapter. One adapter is the [react-i18next](https://react.i18next.com/) package for using i18next with react. It provides a hook for getting the translations with a translation key. The translations itself are saved in the _public/locales_ folder. 

## infrastructure and deployment

The following section describes the main parts of the infrastructure.  

### Gitlab and Gitlab CI

As you surely mentioned before, the project is hosted by Gitlab. Next to the basic features like the git versioning, there is a pipeline created with [Gitlab CI](https://docs.gitlab.com/ee/ci/) too.  

The entrypoint for that is the _.gitlab-ci.yml_ pipeline in the root folder of the project. That file includes the pipeline parts for the frontend, backend und deployment module.

![Different jobs running in the Gitlab CI](images/pipeline.png)

Everytime the pipeline is running in the feature branch, it will execute all unit and integration tests. Next to the tests, the pipeline builds the frontend, backend und the docker image. 

When there is an open issue, the pipelines releases a docker image with the tag _MR-<merge request id> and push it into the docker registry of gitlab.com

When running on the main branch, the pipeline reads the _version_ file in the root of the project and release a docker image with the tag equals the content of the version file.

 The following image explains how the frontend files are delivered to the docker image.
![Deliver frontend files to docker image](images/frontend-files-to-image.png)

First of all, the frontend files are built by the _build_frontend_ job. 
The result will be published as artifact (Gitlab saves the content of the _frontend/build_ folder). Because the _build_backend_ job depends on the _build_frontend_ job, the artifact is included in the repository files before starting the job. 
While running the job two Gradle tasks are executed. The first one (_copyFrontedFiles_ defined in the _backend/build.kts_ file) copies the built frontend into the resource folder. The second one (_bootJar_) builds the backend including the frontend files and creates the _.jar_ file.
The result of the _build_backend_ job is another artifact. In this case the _/backend/build/libs_ folder. This artifact is used of the _build_image_ task.
This is process how the frontend files will be delivered in the docker image.

### Docker

Whenever you have to use multiple software solutions like databases or other services, it is helpful to use an operating system independent solution. In this case you can use [Docker](https://www.docker.com/). For most required services like PostgreSQL or Keycloak, there are docker images available. In that way we can use the same images locally as in our productive systems. 
[Docker Compose](https://docs.docker.com/compose/) provides us additionally to describe our infrastructure in a yaml file. In our case the local infrastructure is defined in the _localInfra/docker-compose.yml_ file and can be started by running `docker compose up -d` in a commandline in the _localInfra_ folder.

An alternative to docker is [podman](https://podman.io/).

### Keycloak 

Nearly every application needs a kind of authentication. For that there are different solutions / methods available (e.g. basic access authentication or [OAuth2](https://auth0.com/de/intro-to-iam/what-is-oauth-2). A very populare open source identity provider is [Keycloak](https://www.keycloak.org/) by Red Hat. It provides OAuth2 / OpenId authentication and is easy to embed to our Spring Boot backend. 

Keycloak has to be configured before it can be used. This is the time to present [Keycloak-Migration](https://mayope.github.io/keycloakmigration/). This is a tool for configuring Keycloak in the same way Liquibase manages the database schema for our application. In the _keycloak-migrations_ folder are all required migrations. First of all we create a new realm (01_add_realm.yml). After that we define global roles for our users (02_add_roles.yml). Our frontend client requires a client entry in our realm. This client is defined in 03_add_clients.yml). For testing we need some users. For that, in the 04_add_dummyData.yml we create two users. The script is only executed, if the environment variable _CREATE_DUMMY_DATA_ is true.

The migration is running automatically when starting the local infra. The result is shown in the Keycloak admin panel. For that you open http://localhost:8181/, click to open the administration panel and login with username _admin_ and password _admin_. After switching the Realm from _Master_ to _app-name_ you can see the hole configuration. 

### PostgreSQL database

Databases are the central part of most web services when the storage of data is required. For that [PostgreSQL](https://www.postgresql.org/) is used in the project. _PostgreSQL_ is an open source relational database which is scalable and widely used in the IT.
There are drivers for Java available. Also, it is easy to use with _Spring Data JPA_ or _Liquibase_.

### OWASP check to monitoring known vulnerable components

Security is an important requirement in every software product. There are a couple of tools which increase your ability to find security issues in your application. One of them is the [OWASP check](https://jeremylong.github.io/DependencyCheck/dependency-check-gradle/index.html). 

It can be executed as Gradle task and checks your Java dependencies for security issues. For that is uses the National Vulnerability Database (NVD) hosted by [NIST](https://nvd.nist.gov/).

The result of running the task is a report of all security issues of your backend dependencies. For configuration, you can use the _owasp-ignore.xml_ file in the _backend_ module.

>
> There is no guarantee that there are no security issues in your application when the owasp check shown no security issues in your dependencies.
> 
